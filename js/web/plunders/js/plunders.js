FoEproxy.addHandler('OtherPlayerService', 'visitPlayer', (data, postData) => {
    LastMapPlayerID = data.responseData['other_player']['player_id'];
    Plunders.CityData = Object.assign({}, ...data.responseData['city_map']['entities'].map((x) => ({ [x.id]: x })));

    if ($('#plunders').length > 0) {
        Plunders.BuildBox();
    } else {
        Plunders.ShowBox();
    }


});


let Plunders = {
    CityData: null,
    Cache: [],


    /**
     * Show or hide the box
     *
     * @constructor
     */
    ShowBox: () => {
        moment.locale(MainParser.Language);

        if ($('#plunders').length === 0) {

            HTML.Box({
                id: 'plunders',
                title: i18n('Boxes.Plunders.Title'),
                auto_close: true,
                dragdrop: true,
                minimize: true
            });

            HTML.AddCssFile('plunders');



            Plunders.BuildBox();

        } else {
            HTML.CloseOpenBox('plunders');
        }
    },

    BuildBox: () => {

        let building = Plunders.CityData,
            rowC = [];
        let type = "";
        let resourcesValue = "";
        let resourcesTypeName = "";
        let playerResource = 0;
        let toPush = false;

        let boostDef = Plunders.getBoostDef(building);

        const regexCrows = new RegExp('R_MultiAge_SummerBonus19*');
        const regexStatue = new RegExp('R_MultiAge_Battlegrounds1*');
        const regexTemple = new RegExp('R_MultiAge_PatrickBonus21*');

        for(let i in building)
        {
            if(building.hasOwnProperty(i))
            {
                if(building[i].type ==='goods' || building[i].type ==='residential' || building[i].type ==='production') {
                    if (building[i]['state']['__class__'] === "ProductionFinishedState" && building[i]['state']['is_motivated'] === false) {
                        //console.log('toPlunder', building[i]);
                        let EntityID = building[i]['cityentity_id'];
                        let CityEntity = MainParser.CityEntities[EntityID];

                        let pA = [],
                            pB = [];
                        let prodTmp = building[i]['state']['current_product']['product'];
                        let prod = null;
                        if(typeof prodTmp !== "undefined") {
                            prod = Object.keys(prodTmp['resources']);
                        }
                        toPush = false;


                        for (let p in prod) {
                            if (prod.hasOwnProperty(p)) {
                                type = prod[p];
                                if (type !== "medals" && type !== "money" && type !== "supplies") {
                                    if(!regexCrows.test(building[i]['cityentity_id']) && !regexStatue.test(building[i]['cityentity_id']) && !regexTemple.test(building[i]['cityentity_id'])) {
                                        resourcesTypeName = GoodsData[type]['name'];
                                        playerResource = ResourceStock[type];
                                        resourcesValue = building[i]['state']['current_product']['product']['resources'][type];
                                        pA.push(resourcesValue + ' ' + resourcesTypeName);
                                        pB.push(playerResource);
                                        toPush = true
                                    }
                                }
                            }
                        }

                        if(toPush===true) {
                            rowC.push('<tr class="' + building[i]['type'] + '">');
                            rowC.push('<td>' + CityEntity['name'] + '</td>');
                            rowC.push('<td>' + pA.join('<br>') + '</td>');
                            rowC.push('<td>' + pB.join('<br>') + '</td>');
                            rowC.push('</tr>');
                        }

                    }
                }
            }
        }

        let h = [];

        h.push('<table class="foe-table">');

        h.push('<thead>');
        h.push('<tr class="defense"><th>Bonus Défense - </th>');
        h.push('<th>Attaque : ' + boostDef["def_att"] + '%</th>');
        h.push('<th>Défense : ' + boostDef["def_def"] + '%</th>');
        h.push('</tr>');
        h.push('<tr>');
        h.push('<th>' + i18n('Plunders.Table.name') + '</th>');
        h.push('<th>' + i18n('Plunders.Table.type') + '</th>');
        h.push('<th>' + i18n('Plunders.Table.pStock') + '</th>');
        h.push('</tr>');
        h.push('</thead>');

        h.push('<tbody>');

        if (rowC.length > 0) {

            h.push( rowC.join('') );
        }
        else {
            h.push('<td colspan="3">' + i18n('Boxes.Plunders.NoPlunders') + '</td>');
        }

        h.push('</tbody>');

        h.push('</table>');

        $('#plundersBody').html(h.join(''));
    },

    getBoostDef: (buildings) => {

        let era = "";
        let def_att = 0;
        let def_def = 0;

        for (let i in buildings) {
            if (buildings[i]['type'] !== 'street') {
                let EID = buildings[i]['cityentity_id'];
                let cEntity = MainParser.CityEntities[EID];
                if (buildings[i]['type'] === 'greatbuilding') {
                    let bonus = buildings[i]['bonus'];
                    if (typeof bonus !== 'undefined') {
                        if (bonus['type'] === "fierce_resistance" || bonus['type'] === "advanced_tactics") {
                            def_att += bonus['value'];
                            def_def += bonus['value'];
                        }
                    }
                } else {
                    let level = buildings[i]['level'];
                    if (typeof cEntity['entity_levels'][level] !== 'undefined') {
                        era = cEntity['entity_levels'][level]['era'];
                    }
                    let abilities = cEntity['abilities'];
                    if (abilities.length > 0) {
                        for (let y in abilities) {
                            for (let [key, value] of Object.entries(abilities[y])) {
                                let typeBoost = null;
                                if (key === "boostHints") {
                                    let boostHints = value;
                                    for (let z in boostHints) {
                                        if (typeof boostHints[z]['boostHintEraMap'][era] !== 'undefined') {
                                            typeBoost = boostHints[z]['boostHintEraMap'][era];
                                        } else if (typeof boostHints[z]['boostHintEraMap']['AllAge'] !== 'undefined') {
                                            typeBoost = boostHints[z]['boostHintEraMap']['AllAge'];
                                        }
                                        if (typeBoost !== null) {
                                            if (typeBoost['type'] === "att_boost_defender") {
                                                def_att += typeBoost['value'];
                                            }
                                            if (typeBoost['type'] === "def_boost_defender") {
                                                def_def += typeBoost['value'];
                                            }
                                        }
                                    }

                                } else if (key === "bonuses") {
                                    let bonuses = value;
                                    for (let a in bonuses) {
                                        let typeBonus = bonuses[a]['boost'];
                                        if (typeof typeBonus !== 'undefined') {
                                            if (typeof typeBonus[era] !== 'undefined') {
                                                typeBoost = typeBonus[era];
                                            } else if (typeof typeBonus['AllAge'] !== 'undefined') {
                                                typeBoost = typeBonus['AllAge'];
                                            }

                                            if(typeBoost !== null) {
                                                if (typeBoost['type'] === "att_boost_defender") {
                                                    def_att += typeBoost['value'];
                                                }
                                                if (typeBoost['type'] === "def_boost_defender") {
                                                    def_def += typeBoost['value'];
                                                }
                                            }
                                        }
                                    }

                                }
                            }
                        }
                    }
                }

            }

        }


        return {'def_att': def_att, 'def_def': def_def};
    }

};