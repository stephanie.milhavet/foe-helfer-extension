/*
 * **************************************************************************************
 *
 * Dateiname:                 productions.js
 * Projekt:                   foe-chrome
 *
 * erstellt von:              Daniel Siekiera <daniel.siekiera@gmail.com>
 * erstellt am:	              22.12.19, 14:31 Uhr
 * zuletzt bearbeitet:       22.12.19, 14:31 Uhr
 *
 * Copyright © 2019
 *
 * **************************************************************************************
 */
FoEproxy.addHandler('OtherPlayerService', 'visitPlayer', (data, postData) => {
    // console.log('visitPlayer', data);
    let cityData = Object.assign({}, ...data.responseData['city_map']['entities'].map((x) => ({ [x.id]: x })));
    // console.log('cityData', cityData);
    ManageGuild.updateData(data.responseData['other_player'], cityData);
});


// FoEproxy.addHandler('BoostService', 'getAllBoosts', (data, postData) => {
//     console.log('Boost',data);
//
//     let att_att = 0,
//         att_def = 0;
//     let boost = data.responseData;
//
//     for (let i in boost) {
//         if(boost[i]['type'] === 'att_boost_attacker') {
//             att_att +=  boost[i]['value'];
//         } else if (boost[i]['type'] === 'def_boost_attacker') {
//             att_def +=  boost[i]['value'];
//         }
//     }
//
//     console.log('bonus', {'att_att': att_att, 'att_def': att_def});
//
// });


FoEproxy.addHandler('StartupService', 'getData', (data, postData) => {
    // console.log('startup',data);
    let members = data.responseData['socialbar_list'];
    ManageGuild.Self['cityData'] = Object.assign({}, ...data.responseData['city_map']['entities'].map((x) => ({ [x.id]: x })));

    for (let i in members) {
        if(members[i]['is_self']) {
            ManageGuild.Self['player'] = members[i];
        }
    }
});

FoEproxy.addHandler('ClanService', 'getTreasuryLogs', (data) => {
    // ManageGuild.HandleNewLogs(data);
    // console.log('getTreasuryLogs',data);
});


// FoEproxy.addHandler('all', 'all', (data, postData) => {
//     console.log('all',data);
// });


FoEproxy.addHandler('ClanService', 'getTreasury', (data, postData) => {
    // console.log('clan service treasury',data);
});

FoEproxy.addHandler('ClanService', 'all', (data, postData) => {
     // console.log('clan service all',data);
});


let ManageGuild = {

    Members: [],
    Self: {},
    TreasuryLogs: [],
    TreasuryIn: [],
    PlayableEras: [
        'BronzeAge',
        'IronAge',
        'EarlyMiddleAge',
        'HighMiddleAge',
        'LateMiddleAge',
        'ColonialAge',
        'IndustrialAge',
        'ProgressiveEra',
        'ModernEra',
        'PostModernEra',
        'ContemporaryEra',
        'TomorrowEra',
        'FutureEra',
        'ArcticFuture',
        'OceanicFuture',
        'VirtualFuture',
        'SpaceAgeMars',
        'SpaceAgeAsteroidBelt',
        'SpaceAgeVenus'
    ],
    PlayerBoxContent: [],
    BuildingsBoxContent: [],
    TreasuryBoxContent: [],

    updateData: (player, cityData) => {
        // console.log('player', player);

        if (localStorage.getItem('ManageGuild.Members') !== null) {
            ManageGuild.Members = JSON.parse(localStorage.getItem('ManageGuild.Members'));
        }


        if (player['is_guild_member']) {

            let playerId = player['player_id'];
            let score = player['score'];
            let era = player['era'];
            let isActive = player['is_active'];
            let playerName = player['name'];
            let playerAvatar = player['avatar'];

            let buildings = ManageGuild.getBuildings(cityData, era);
            let bonus = ManageGuild.getBonus(cityData);


            let playerData = {
                playerId: playerId,
                playerName: playerName,
                playerAvatar: playerAvatar,
                era: era,
                score: score,
                isActive: isActive,
                bonus: bonus,
                buildings: buildings
            };
            let index = ManageGuild.Members.findIndex(x => x.playerId === player['player_id']);
            if (index === -1) {
                ManageGuild.Members.push(playerData);
            } else {
                ManageGuild.Members[index] = playerData;
            }



        } else {
            let index = ManageGuild.Members.findIndex(x => x.playerId === player['player_id']);
            if (index > -1) {
                ManageGuild.Members.splice(index, 1);
            }
        }

        ManageGuild.Members.sort(function (a, b) {
            let scoreA = a['score'];
            let scoreB = b['score'];
            if (scoreA > scoreB) {
                return -1;
            }
            if (scoreA < scoreB) {
                return 1;
            }
            // names must be equal
            return 0;
        });
        localStorage.setItem('ManageGuild.Members', JSON.stringify(ManageGuild.Members));

        ManageGuild.updateTreasuryIn();

        // console.log('Members', ManageGuild.Members);
    },
    getBuildings: (buildings, era) => {
        let clanGM = [];
        let clanBuildings = [];

        for (let i in buildings) {
            if (buildings[i]['type'] !== 'street') {
                // console.log('building', buildings[i]);
                let EID = buildings[i]['cityentity_id'];
                let cEntity = MainParser.CityEntities[EID];
                // console.log('cEntity', cEntity);
                if (buildings[i]['type'] === 'greatbuilding') {
                	if(typeof buildings[i]['state']['current_product'] !== 'undefined') {
                        if (buildings[i]['state']['current_product']['asset_name'] === "clan_goods") {
                            let nbGoods = buildings[i]['state']['current_product']['goods'][0]['value'] * 5;
                            let gm = {
                                eid: EID,
                                name: cEntity['name'],
                                level: buildings[i]['level'],
                                era: era,
                                nbGoods: nbGoods
                            };

                            clanGM.push(gm);
                        }
                    }
                } else {
                    let abilities = cEntity['abilities'];
                    if (abilities.length > 0) {
                        for (let y in abilities) {
                            for (let [key, value] of Object.entries(abilities[y])) {
                                if(value === 'AddResourcesToGuildTreasuryAbility') {
                                	let nbGoods = abilities[y]['additionalResources']['AllAge']['resources']['all_goods_of_age'];
                                	// regexp sur l'eid
                                	let level = "";

                                    let buildLevel = buildings[i]['level'];
                                    let era = "";
                                    if (typeof cEntity['entity_levels'][buildLevel] !== 'undefined') {
                                        era = cEntity['entity_levels'][buildLevel]['era'];
                                    }
                                	let bat = {
                                		eid: EID,
										name: cEntity['name'],
										level: level,
										era: era,
										nbGoods: nbGoods
									};

                                    clanBuildings.push(bat);

								}
                            }
                        }
                    }
                }
            }
        }
        return {'clanGM': clanGM, 'clanBuildings': clanBuildings};
    },
    getBonus: (buildings) => {
        let era = "";
        let att_att = 0;
        let att_def = 0;

        // console.log('buildings', buildings);

        for (let i in buildings) {
            if (buildings[i]['type'] !== 'street') {
                let EID = buildings[i]['cityentity_id'];
                let cEntity = MainParser.CityEntities[EID];
                // console.log('cEntity', cEntity);
                if (buildings[i]['type'] === 'greatbuilding') {
                    let bonus = buildings[i]['bonus'];
                    if (typeof bonus !== 'undefined') {
                        if (bonus['type'] === "military_boost" || bonus['type'] === "advanced_tactics") {
                            att_att += bonus['value'];
                            att_def += bonus['value'];
                        }
                    }
                } else {
                    let level = buildings[i]['level'];
                    if (typeof cEntity['entity_levels'][level] !== 'undefined') {
                        era = cEntity['entity_levels'][level]['era'];
                    }
                    let abilities = cEntity['abilities'];
                    if (abilities.length > 0) {
                        for (let y in abilities) {
                            for (let [key, value] of Object.entries(abilities[y])) {
                                let typeBoost = null;
                                if (key === "boostHints") {
                                    let boostHints = value;
                                    for (let z in boostHints) {
                                        if (typeof boostHints[z]['boostHintEraMap'][era] !== 'undefined') {
                                            typeBoost = boostHints[z]['boostHintEraMap'][era];
                                        } else if (typeof boostHints[z]['boostHintEraMap']['AllAge'] !== 'undefined') {
                                            typeBoost = boostHints[z]['boostHintEraMap']['AllAge'];
                                        }
                                        if (typeBoost !== null) {
                                            if (typeBoost['type'] === "att_boost_attacker") {
                                                att_att += typeBoost['value'];
                                            }
                                            if (typeBoost['type'] === "def_boost_attacker") {
                                                att_def += typeBoost['value'];
                                            }
                                        }
                                    }
                                } else if (key === "bonuses") {
                                    let bonuses = value;
                                    for (let a in bonuses) {
                                        let typeBonus = bonuses[a]['boost'];
                                        if (typeof typeBonus !== 'undefined') {
                                            if (typeof typeBonus[era] !== 'undefined') {
                                                typeBoost = typeBonus[era];
                                            } else if (typeof typeBonus['AllAge'] !== 'undefined') {
                                                typeBoost = typeBonus['AllAge'];
                                            }

                                            if(typeBoost !== null) {
                                                if (typeBoost['type'] === "att_boost_attacker") {
                                                    att_att += typeBoost['value'];
                                                }
                                                if (typeBoost['type'] === "def_boost_attacker") {
                                                    att_def += typeBoost['value'];
                                                }
                                            }
                                        }
                                    }
                                } else if (key === "bonusGiven") {
									let typeBonus = value['boost'];
									if (typeof typeBonus !== 'undefined') {
										if (typeof typeBonus[era] !== 'undefined') {
											typeBoost = typeBonus[era];
										} else if (typeof typeBonus['AllAge'] !== 'undefined') {
											typeBoost = typeBonus['AllAge'];
										}

										if (typeBoost !== null) {
											if (typeBoost['type'] === "att_boost_attacker") {
												att_att += typeBoost['value'];
											}
											if (typeBoost['type'] === "def_boost_attacker") {
                                                if(EID === "D_MultiAge_SoccerBonus20Rousioi") {
                                                    att_def += typeBoost['value'] *2;
                                                } else {
                                                    att_def += typeBoost['value'];
                                                }
											}
										}
									}
                                }
                            }
                        }
                    }
                }
            }
        }

        return {'att_att': att_att, 'att_def': att_def};
    },
    updateTreasuryIn: () => {
        if (localStorage.getItem('ManageGuild.Members') !== null) {
            ManageGuild.Members = JSON.parse(localStorage.getItem('ManageGuild.Members'));
        }
        // console.log('ManageGuildMembers', ManageGuild.Members);

        // if (localStorage.getItem('ManageGuild.TreasuryIn') !== null) {
        //     ManageGuild.TreasuryIn = JSON.parse(localStorage.getItem('ManageGuild.TreasuryIn'));
        // }


        for(let mb in ManageGuild.Members) {
            let buildings = [];

            let member = ManageGuild.Members[mb]['playerId'];
            for (let [key, value] of Object.entries(ManageGuild.Members[mb]['buildings'])) {
                if (value.length > 0) {
                    value.forEach(function(v) {
                        buildings.push(v);
                    })
                }
            }
            // buildings.reduce((acc, it) => acc.concat(it), []);
            // console.log('buildings', buildings);
            ManageGuild.PlayableEras.forEach(function(pEras) {
                let items = buildings.filter(item => item.era === pEras);
                if (items.length > 0) {
                    // console.log('items', items);
                    let nbGoodsDay = items.reduce(function(acc, val) {
                        return acc + val.nbGoods;
                    },0);
                    let pTreasuryEra = {
                        playerId: member,
                        era: pEras,
                        goodsPerDay: nbGoodsDay,
                        goodsPerWeek: nbGoodsDay * 7
                    };

                    let keyValues = [
                        { 'key': 'playerId', 'value': member },
                        { 'key': 'era', 'value': pEras }];
                    let index = MainParser.indexOfObjectArray(ManageGuild.TreasuryIn, keyValues);
                    if (index === -1) {
                        ManageGuild.TreasuryIn.push(pTreasuryEra);
                    } else {
                        ManageGuild.TreasuryIn[index] = pTreasuryEra;
                    }
                }
            });
        }


        localStorage.setItem('ManageGuild.TreasuryIn', JSON.stringify(ManageGuild.TreasuryIn));

        // console.log('TreasuryIn', ManageGuild.TreasuryIn);
    },
    HandleNewLogs: (Logs) => {
        localStorage.removeItem('ManageGuild.TreasuryLogs');

        // if (localStorage.getItem('ManageGuild.TreasuryLogs') !== null) {
        //     ManageGuild.TreasuryLogs = JSON.parse(localStorage.getItem('ManageGuild.TreasuryLogs'));
        // }
        //
        // let LogArray = Logs['responseData']['logs'];
        // for (let i = 0; i < LogArray.length; i++) {
        //     ManageGuild.TreasuryLogs[ManageGuild.TreasuryLogs.length] = LogArray[i];
        // }
        //
        // localStorage.setItem('ManageGuild.TreasuryLogs', JSON.stringify(ManageGuild.TreasuryLogs));
        //
        // console.log('TreasuryLogs', ManageGuild.TreasuryLogs);
    },
    /**
     * HTML Box erstellen und einblenden
     */
    showBox: () => {
        moment.locale(i18n('Local'));

        if ($('#ManageGuild').length === 0) {

            HTML.Box({
                'id': 'ManageGuild',
                'title': i18n('Menu.ManageGuild.Title'),
                'auto_close': true,
                'dragdrop': true,
                'minimize': true,
				'resize':  true,
                'settings': 'ManageGuild.ShowExportButton()'
            });

            HTML.AddCssFile('manage-guild');


        } else {
            HTML.CloseOpenBox('ManageGuild');
        }

        ManageGuild.CalcBody();
    },

    CalcBody: () =>{
        let body = [];
        body.push('<div class="manage-guild-tabs tabs"><ul class="horizontal">');
        body.push('<li class="playerInfos game-cursor"><a href="#playerInfos" class="game-cursor"><span>&nbsp;</span></a></li>');
        body.push('<li class="treasuryIn game-cursor"><a href="#treasuryIn" class="game-cursor"><span>&nbsp;</span></a></li>');
        body.push('<li class="cbgCosts game-cursor"><a href="#cbgCosts" class="game-cursor"><span>&nbsp;</span></a></li>');
        body.push('</ul>');

        body = ManageGuild.getPlayerInfos(body);
        body = ManageGuild.getTreasuryIn(body);
        // body = ManageGuild.getCBGCosts(body);



        body.push('</div>');

        $('#ManageGuild').find('#ManageGuildBody').html(body.join('')).promise().done(function () {

            $('.manage-guild-tabs').tabslet();

        });

    },

    getCBGCosts: async (body) => {

        let rowC = [];
        const t = await IndexDB.db.statsTreasureClanH.orderBy('date').toArray();

        if (localStorage.getItem('GildFights.BuildingsData') !== null) {
            GildFights.BuildingsData = JSON.parse(localStorage.getItem('GildFights.BuildingsData'));
        }

        console.log('GildFights.BuildingsData', GildFights.BuildingsData);
        let treasury = t[t.length-1]['resources'];
        console.log('treasury', treasury);

        for(let i in GildFights.BuildingsData) {
            // if (!GildFights.BuildingsData.hasOwnProperty(i)) {
            //     break;
            // }
            let buildingsData = GildFights.BuildingsData[i];
            console.log('buildingsData', buildingsData);
            let pA = [];
            let costs = Object.keys(buildingsData['costs']);
            for (let y in costs) {
                let type = costs[y];
                let resourcesName = GoodsData[type]['name'];
                let clanResource = '<span class="text-success">' + treasury[type] + '</span>';
                let resourcesValue = '<span class="text-danger">' + buildingsData['costs'][type] + '</span>';
                pA.push(resourcesValue + ' / ' + clanResource + ' ' +resourcesName);
                console.log('pa', pA);
            }

            rowC.push(`<div class="row col-12 table-body">`);
            rowC.push('<div class="tdCol col-5">' + buildingsData['province_name'] + '</div>');
            rowC.push('<div class="tdCol col-3">' + buildingsData['province_slots'] + '</div>');
            rowC.push('<div class="tdCol col-3">' + pA.join('<br>') + '</div>');
            rowC.push('</div>');
        }


        body.push('<div id="cbgCosts" style="display:none;" class="container-fluid foe-table">');


        body.push('<div class="row col-12 table-head dark-bg">');
        body.push('<div class="tdCol col-1">Secteur</div>');
        body.push('<div class="tdCol col-1">Nb de camps</div>');
        body.push('<div class="tdCol col-3">Coûts</div>');

        body.push('</div>');

        if (rowC.length > 0) {

            body.push( rowC.join('') );
        }
        else {
            body.push('<div class="row"><div class="text-center tdCol col-sm-12">Aucune donnée</div></div>');
        }

        body.push('</div>');

        return body;

    },

    getTreasuryIn: (body) => {

        let rowC = [];

        if (localStorage.getItem('ManageGuild.TreasuryIn') !== null) {
            ManageGuild.TreasuryIn = JSON.parse(localStorage.getItem('ManageGuild.TreasuryIn'));
        }

        ManageGuild.TreasuryBoxContent = [];

        ManageGuild.TreasuryBoxContent.push({
            era: 'Âge',
            nbGoodsDay: 'Ressources par Jour',
            nbGoodsWeek: 'Ressources par Semaine'
        });

        console.log('TreasuryIn', ManageGuild.TreasuryIn);

            ManageGuild.PlayableEras.forEach(function(pEras) {
                let items = ManageGuild.TreasuryIn.filter(item => item.era === pEras);
                if (items.length > 0) {
                    // console.log('items', items);
                    let nbGoodsDay = items.reduce(function(acc, val) {
                        return acc + val.goodsPerDay;
                    },0);
                    let nbGoodsWeek = items.reduce(function(acc, val) {
                        return acc + val.goodsPerWeek;
                    },0);
                    ManageGuild.TreasuryBoxContent.push({
                        era: i18n('Eras.' + Technologies.Eras[pEras]),
                        nbGoodsDay: nbGoodsDay,
                        nbGoodsWeek: nbGoodsWeek
                    });

                    rowC.push(`<div class="row col-6 table-body">`);
                    rowC.push('<div class="tdCol col-5">' + i18n('Eras.' + Technologies.Eras[pEras]) + '</div>');
                    rowC.push('<div class="tdCol col-3">' + nbGoodsDay.toLocaleString('fr-FR') + '</div>');
                    rowC.push('<div class="tdCol col-3">' + nbGoodsWeek.toLocaleString('fr-FR') + '</div>');
                    rowC.push('</div>');

                }
            });





        body.push('<div  id="treasuryIn" style="display:none;" class="container-fluid foe-table">');


        body.push('<div class="row col-6 table-head dark-bg">');
        body.push('<div class="tdCol col-5">Âge</div>');
        body.push('<div class="tdCol col-3">Ressources par Jour</div>');
        body.push('<div class="tdCol col-3">Ressources par Semaine</div>');

        body.push('</div>');

        if (rowC.length > 0) {

            body.push( rowC.join('') );
        }
        else {
            body.push('<div class="row"><div class="text-center tdCol col-sm-12">Aucune donnée</div></div>');
        }

        body.push('</div>');

        return body;

    },

    /**
     * Aktualisiert den Inhalt
     */
    getPlayerInfos: (body) => {
        let rowC = [];
        ManageGuild.updateData(ManageGuild.Self['player'], ManageGuild.Self['cityData']);

        if (localStorage.getItem('ManageGuild.Members') !== null) {
            ManageGuild.Members = JSON.parse(localStorage.getItem('ManageGuild.Members'));
        }

        ManageGuild.PlayerBoxContent = [];

        ManageGuild.PlayerBoxContent.push({
            rang: 'Rang',
            name: 'Nom',
            score: 'Points',
            era: 'Âge',
            att: '% Att',
            def: '% Déf'
        });

        ManageGuild.BuildingsBoxContent = [];

        ManageGuild.BuildingsBoxContent.push({
            name: 'Nom',
            building: 'Bâtiment',
            level: 'Niveau',
            era: 'Âge',
            nbGoods: 'Ressources'
        });


        // einzelne Güterarten durchsteppen
        for(let mb in ManageGuild.Members) {
        	let member = ManageGuild.Members[mb]['playerId'];

            let buildings = ManageGuild.Members[mb]['buildings'];
            let clanGM = buildings['clanGM'];
            let clanBuildings = buildings['clanBuildings'];
            let rowA = [];
            let rowB = [];
            let playerEra = ManageGuild.Members[mb]['era'];

            if (typeof clanGM !== 'undefined') {
                rowA.push('<div class="row">');
                rowA.push('<div class="text-center dark-bg tdCol col-sm-12">Grands Monuments</div></div>');
                rowA.push('<div class="row">');
                rowA.push('<div class="col tdCol dark-bg">Bâtiment</div><div class="col tdCol dark-bg">Niveau</div>' +
                    '<div class="col tdCol dark-bg">Âge</div><div class="col tdCol dark-bg">Ressources</div></div>');
                for (let g in clanGM) {
                    let gmEra = clanGM[g]['era'];

                    ManageGuild.BuildingsBoxContent.push({
                        name: ManageGuild.Members[mb]['playerName'],
                        building: clanGM[g]['name'],
                        level: clanGM[g]['level'],
                        era: i18n('Eras.' + Technologies.Eras[gmEra]),
                        nbGoods: clanGM[g]['nbGoods']
                    });
                    rowA.push('<div class="row">');
                    rowA.push('<div class="col tdCol">' + clanGM[g]['name'] + '</div>');
                    rowA.push('<div class="col tdCol">' + clanGM[g]['level'] + '</div>');
                    rowA.push('<div class="col tdCol">' + i18n('Eras.' + Technologies.Eras[gmEra]) + '</div>');
                    rowA.push('<div class="col tdCol">' + clanGM[g]['nbGoods'] + '</div>');
                    rowA.push('</div>');
                }
            }

            if (typeof clanBuildings !== 'undefined') {

                rowB.push('<div class="row">');
                rowB.push('<div class="text-center dark-bg tdCol col-sm-12">Bâtiments spéciaux</div></div>');
                rowB.push('<div class="row">');
                rowB.push('<div class="col tdCol dark-bg">Bâtiment</div><div class="col tdCol dark-bg">Niveau</div>' +
                    '<div class="col tdCol dark-bg">Âge</div><div class="col tdCol dark-bg">Ressources</div></div>');

                for (let b in clanBuildings) {
                    let buildingEra = clanBuildings[b]['era'];

                    ManageGuild.BuildingsBoxContent.push({
                        name: ManageGuild.Members[mb]['playerName'],
                        building: clanBuildings[b]['name'],
                        level: clanBuildings[b]['level'],
                        era: i18n('Eras.' + Technologies.Eras[buildingEra]),
                        nbGoods: clanBuildings[b]['nbGoods']
                    });
                    rowB.push('<div class="row">');
                    rowB.push('<div class="tdCol col">' + clanBuildings[b]['name'] + '</div>');
                    rowB.push('<div class="tdCol col">' + clanBuildings[b]['level'] + '</div>');
                    rowB.push('<div class="tdCol col">' + i18n('Eras.' + Technologies.Eras[buildingEra]) + '</div>');
                    rowB.push('<div class="tdCol col">' + clanBuildings[b]['nbGoods'] + '</div>');
                    rowB.push('</div>');

                }
            }

            ManageGuild.PlayerBoxContent.push({
                rang: '#' + (parseInt(mb) + 1),
                name: ManageGuild.Members[mb]['playerName'],
                score: ManageGuild.Members[mb]['score'],
                era: i18n('Eras.' + Technologies.Eras[playerEra]),
                att: ManageGuild.Members[mb]['bonus']['att_att'],
                def: ManageGuild.Members[mb]['bonus']['att_def']
            });

            rowC.push(`<div class="row col-12 table-body manageguild-accordion ${member}" onclick="ManageGuild.TogglePlayer('${member}')">`);
            rowC.push(`<div class="tdCol col-1">#` + (parseInt(mb) + 1) + `</div>`);
            rowC.push(`<div class="tdCol col-1"><img style="max-width: 22px" src="${MainParser.InnoCDN + 'assets/shared/avatars/' + MainParser.PlayerPortraits[ManageGuild.Members[mb]['playerAvatar']]}.jpg" ></div>`);
            rowC.push('<div class="tdCol col-3">' + ManageGuild.Members[mb]['playerName'] + '</div>');
            rowC.push('<div class="tdCol col-2">' + ManageGuild.Members[mb]['score'].toLocaleString('fr-FR') + '</div>');
            rowC.push('<div class="tdCol col-3">' + i18n('Eras.' + Technologies.Eras[playerEra]) + '</div>');
            rowC.push('<div class="tdCol col-1">' + ManageGuild.Members[mb]['bonus']['att_att'] + '</div>');
            rowC.push('<div class="tdCol col-1">' + ManageGuild.Members[mb]['bonus']['att_def'] + '</div>');



            let m = [];
            m.push(`<div class="w-100 container-fluid manageguild-body ${member}-body">`);
            if (rowA.length > 0) {

                m.push( rowA.join('') );
            }
            else {
                m.push('<div class="row"><div class="text-center tdCol col-sm-12">Aucun Grands Monuments</div></div>');
            }

            if (rowB.length > 0) {

                m.push( rowB.join('') );
            }
            else {
                m.push('<div class="row"><div class="text-center tdCol col-sm-12">Aucun Bâtiments spéciaux</div></div>');
            }
            m.push(`</div>`);

            rowC.push( m.join(''));
            rowC.push('</div>');

        }

        body.push('<div  id="playerInfos" style="display:block;" class="container-fluid foe-table">');

        body.push('<div class="row col-12 table-head dark-bg">');
        body.push('<div class="tdCol col-1">Rang</div>');
        body.push('<div class="tdCol col-1"></div>');
        body.push('<div class="tdCol col-3">Nom</div>');
        body.push('<div class="tdCol col-2">Points</div>');
        body.push('<div class="tdCol col-3">Âge</div>');
        body.push('<div class="tdCol col-1">% Attaque</div>');
        body.push('<div class="tdCol col-1">% Défense</div>');

        body.push('</div>');

        if (rowC.length > 0) {

            body.push( rowC.join('') );
        }
        else {
            body.push('<div class="row"><div class="text-center tdCol col-sm-12">Aucune donnée</div></div>');
        }

        body.push('</div>');

        return body;
    },

    TogglePlayer: (member)=> {
        let $this = $(`.${member}`),
            isOpen = $this.hasClass('open');

        $('.manageguild-accordion').removeClass('open');

        if(!isOpen){
            $this.addClass('open');
        }
    },
    ShowExportButton: () => {
        let c = `<p class="text-center"><button class="btn btn-default" onclick="ManageGuild.SettingsExportAll()">${i18n('Boxes.Gildfights.ExportCSV')}</button></p>`;

        //c += `<p class="text-center"><button class="btn btn-default" onclick="GildFights.SettingsExport('json', '` + origin+ `')">${i18n('Boxes.Gildfights.ExportJSON')}</button></p>`;

        // insert into DOM
        $('#ManageGuildSettingsBox').html(c);



    },

    SettingsExportAll: ()=> {

        ManageGuild.SettingsExport('player');
        ManageGuild.SettingsExport('buildings');
        ManageGuild.SettingsExport('treasury');


        $(`#ManageGuildSettingsBox`).fadeToggle('fast', function(){
            $(this).remove();
        });


    },


    SettingsExport: (type)=> {

        let blob, file;

        let csv = [];

        if (type === 'player') {
            for (let i in ManageGuild.PlayerBoxContent) {
                if (!ManageGuild.PlayerBoxContent.hasOwnProperty(i)) {
                    break;
                }

                let r = ManageGuild.PlayerBoxContent[i];
                csv.push(`${r['rang']};${r['name']};${r['score']};${r['era']};${r['att']};${r['def']}`);
            }

            blob = new Blob(['\uFEFF', csv.join('\r\n')], {type: 'text/csv;charset=utf-8'});
            file = `guildPlayers-${ExtWorld}.csv`;
        }
        else if (type === 'buildings') {
            for (let i in ManageGuild.BuildingsBoxContent) {
                if (!ManageGuild.BuildingsBoxContent.hasOwnProperty(i)) {
                    break;
                }

                let r = ManageGuild.BuildingsBoxContent[i];
                csv.push(`${r['name']};${r['building']};${r['level']};${r['era']};${r['nbGoods']}`);
            }

            blob = new Blob(['\uFEFF', csv.join('\r\n')], {type: 'text/csv;charset=utf-8'});
            file = `guildBuildings-${ExtWorld}.csv`;
        } else if (type === 'treasury') {
            for (let i in ManageGuild.TreasuryBoxContent) {
                if (!ManageGuild.TreasuryBoxContent.hasOwnProperty(i)) {
                    break;
                }

                let r = ManageGuild.TreasuryBoxContent[i];
                csv.push(`${r['era']};${r['nbGoodsDay']};${r['nbGoodsWeek']}`);
            }

            blob = new Blob(['\uFEFF', csv.join('\r\n')], {type: 'text/csv;charset=utf-8'});
            file = `guildTreasury-${ExtWorld}.csv`;
        }

        MainParser.ExportFile(blob, file);


    }


};
